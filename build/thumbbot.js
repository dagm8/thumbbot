"use strict";

var _prototypeProperties = function (child, staticProps, instanceProps) {
  if (staticProps) Object.defineProperties(child, staticProps);
  if (instanceProps) Object.defineProperties(child.prototype, instanceProps);
};

/**
 * Module dependencies
 */

var request = require("co-request");
var Image = require("magician");
var exec = require("co-exec");

var util = require("./util");


/**
 * Thumbbot
 */

var Thumbbot = (function () {
  function Thumbbot(path) {
    this.path = path;
    this.options = {};
  }

  _prototypeProperties(Thumbbot, null, {
    original: {
      value: function original() {
        if (this.type() !== "image") {
          return {};
        }
        var image = new Image(this.path);
        return image.meta();
      },
      writable: true,
      enumerable: true,
      configurable: true
    },
    format: {
      value: function format(format) {
        this.options.format = format;

        return this;
      },
      writable: true,
      enumerable: true,
      configurable: true
    },
    width: {
      value: function width(width) {
        if (!this.options.resize) this.options.resize = {};

        this.options.resize.width = width;

        return this;
      },
      writable: true,
      enumerable: true,
      configurable: true
    },
    height: {
      value: function height(height) {
        if (!this.options.resize) this.options.resize = {};

        this.options.resize.height = height;

        return this;
      },
      writable: true,
      enumerable: true,
      configurable: true
    },
    resize: {
      value: function resize(width, height) {
        this.options.resize = { width: width, height: height };

        return this;
      },
      writable: true,
      enumerable: true,
      configurable: true
    },
    window: {
      value: function window(width, height) {
        this.options.window = { width: width, height: height };

        return this;
      },
      writable: true,
      enumerable: true,
      configurable: true
    },
    seek: {
      value: function seek(position) {
        this.options.seek = position;

        return this;
      },
      writable: true,
      enumerable: true,
      configurable: true
    },
    crop: {
      value: function crop(x, y, width, height) {
        this.options.crop = { x: x, y: y, width: width, height: height };

        return this;
      },
      writable: true,
      enumerable: true,
      configurable: true
    },
    disable: {
      value: function disable(feature) {
        if (!this.options.disable) this.options.disable = {};

        this.options.disable[feature] = true;
      },
      writable: true,
      enumerable: true,
      configurable: true
    },
    type: {
      value: function type() {
        var extension = util.parseExtension(this.path);

        switch (extension) {
          case "mp4":
          case "3gp":
          case "mov":
          case "avi":
          case "m4v":
            return "video";

          case "jpeg":
          case "jpg":
          case "png":
          case "gif":
          case "tiff":
            return "image";

          default:
            return "page";
        }
      },
      writable: true,
      enumerable: true,
      configurable: true
    },
    saveVideo: {
      value: function saveVideo(destPath) {
        var seek = this.options.seek || "00:00:01";
        if (!this.options.resize) this.options.resize = {};
        var width = this.options.resize.width || 250;
        var command = "ffmpeg -i \"" + this.path + "\" -ss \"" + seek + "\" -filter:v scale=\"" + width + ":-1\" -vframes 1 \"" + destPath + "\"";

        return exec(command);
      },
      writable: true,
      enumerable: true,
      configurable: true
    },
    savePage: {
      value: function savePage(destPath) {
        var options = Object.assign({}, this.options);

        options.url = encodeURIComponent(this.path);
        options.destPath = destPath;
        options = JSON.stringify(options);

        var command = ["phantomjs", "--disk-cache=true", "--ignore-ssl-errors=true", "--web-security=false", "--ssl-protocol=TLSv1", __dirname + "/render.js", "'" + options + "'"].join(" ");

        return exec(command);
      },
      writable: true,
      enumerable: true,
      configurable: true
    },
    saveImage: {
      value: function saveImage(destPath) {
        var extension = util.parseExtension(this.path);
        var path = extension === "gif" ? this.path + "[0]" : this.path;
        var image = new Image(path);

        var options = this.options;
        var resize = options.resize;
        var crop = options.crop;


        if (resize) {
          image.resize(resize.width, resize.height);
          image.fit(options.fit || "clip");
        }

        if (crop) {
          image.crop(crop.x, crop.y, crop.width, crop.height);
        }

        return image.save(destPath);
      },
      writable: true,
      enumerable: true,
      configurable: true
    },
    save: {
      value: function* save(destPath) {
        var type = this.type();

        if (!destPath) {
          destPath = util.tmpPath(this.options.format);
        }

        switch (type) {
          case "video":
            yield this.saveVideo(destPath);
            break;

          case "image":
            yield this.saveImage(destPath);
            break;

          case "page":
            yield this.savePage(destPath);
            break;

          default:
            throw new Error("Uknown file type given to Thumbbot");
        }

        return new Image(destPath);
      },
      writable: true,
      enumerable: true,
      configurable: true
    }
  });

  return Thumbbot;
})();




/**
 * Module exports
 */

module.exports = Thumbbot;
